def setup():
    ox_model = OX_model()
    ox_viewer = OX_viewer(ox_model)
    ox_controller = OX_controller_one_input(ox_model, ox_viewer)
    ox_controller.start_game()


class OX_model():

    def __init__(self):
        self.table = [['#', '#', '#'],  # table 3*3,'#' mean normal value
                      ['#', '#', '#'],
                      ['#', '#', '#']]
        self.count_round = 0  # count everytime when char was fill

    def get_value_in_table(self, x, y):
        return self.table[y][x]

    def get_size_x(self, y):
        return len(self.table[y])

    def get_size_y(self):
        return len(self.table)

    def check_filled(self, x, y):  # that point has filled?
        return (self.table[y][x] == '#')

    def check_player_turn(self):  # that turn is O or X turn ?
        return (self.count_round % 2)

    def get_count_round(self):
        return self.count_round

    def increase_count_round(self, value):  # input value for increase
        self.count_round = self.count_round + value

    def check_someonewin(self):
        if(self.count_round >= 5):
            if(self.table[1][1] != '#'):
                if(self.table[0][0] == self.table[1][1]):
                    if(self.table[2][2] == self.table[1][1]):
                        return True
                if(self.table[0][1] == self.table[1][1]):
                    if(self.table[2][1] == self.table[1][1]):
                        return True
                if(self.table[0][2] == self.table[1][1]):
                    if(self.table[2][0] == self.table[1][1]):
                        return True
                if(self.table[1][0] == self.table[1][1]):
                    if(self.table[1][2] == self.table[1][1]):
                        return True
            if(self.table[0][2] != '#'):
                if(self.table[0][1] == self.table[0][2]):
                    if(self.table[0][0] == self.table[0][2]):
                        return True
                if(self.table[1][2] == self.table[0][2]):
                    if(self.table[2][2] == self.table[0][2]):
                        return True
            if(self.table[2][0] != '#'):
                if(self.table[1][0] == self.table[2][0]):
                    if(self.table[0][0] == self.table[2][0]):
                        return True
                if(self.table[2][1] == self.table[2][0]):
                    if(self.table[2][2] == self.table[2][0]):
                        return True
            if(self.count_round == 9):
                self.count_round = 10
                return True
        return False

    def fill_table(self, x, y, mask):  # change value in point
        self.table[y][x] = mask


class OX_viewer():

    def __init__(self, model):
        self.model = model

    def show_table(self):
        ''' show table in present at console
          example  # # #
                   # O X
                   O X #  '''
        count_y = 0
        len_table_y = self.model.get_size_y()
        while(count_y < len_table_y):
            count_x = 0
            len_table_x = self.model.get_size_x(count_y)
            while(count_x < len_table_x):
                value = self.model.get_value_in_table(count_x, count_y)
                print(value, end=' ')
                count_x = count_x + 1
            print('')
            count_y = count_y + 1

    def show_x_win(self):
        print('*****************************')
        print('******** X IS WINNER ********')
        print('*****************************')

    def show_o_win(self):
        print('*****************************')
        print('******** O IS WINNER ********')
        print('*****************************')

    def show_draw(self):
        print('*****************************')
        print('*********** DRAW ************')
        print('*****************************')

    def show_x_turn(self):
        print('*********  X TURN ***********')

    def show_o_turn(self):
        print('*********  O TURN ***********')

    def show_error_non_number(self):
        print('Hey you dont give any number!!! please try agian')

    def show_error_out_list(self):
        print('Hey your number is out of table please try agian')

    def show_error_filled(self):
        print('your point has been filled please try anoter point')


class OX_controller():

    def __init__(self, model, view):
        self.model = model
        self.view = view

    def start_game(self):
        while(True):  # loop for play
            self.model.increase_count_round(1)
            self.show_player_turn()
            self.view.show_table()  # export table to method
            self.player_selection()
            if(self.model.check_someonewin()):
                break
        if(self.model.get_count_round() > 9):  # condition check draw
            self.view.show_table()  # export table to method
            self.view.show_draw()
        else:
            self.view.show_table()  # export table to method
            if(self.model.check_player_turn() == 0):
                self.view.show_o_win()
            else:
                self.view.show_x_win()

    def show_player_turn(self):
        if(self.model.check_player_turn() == 0):
            self.view.show_o_turn()
        else:
            self.view.show_x_turn()


class OX_controller_two_input(OX_controller):

    def __init__(self, model, view):
        super().__init__(self, model, view)

    def player_selection(self):
        while(True):
            x = self.player_x_select() - 1
            y = self.player_y_select() - 1
            if(self.model.check_filled(x, y)):
                if(self.model.check_player_turn() == 0):
                    self.model.fill_table(x, y, 'O')
                else:
                    self.model.fill_table(x, y, 'X')
                break
            else:
                self.view.show_error_filled()

    def player_x_select(self):
        while(True):
            while(True):  # try over and over untill player fill anwser
                try:
                    x = int(input('Please select x point : '))
                    break
                except ValueError:
                    self.view.show_error_non_number()
            if(x > 3 or x < 1):  # check error out of index
                self.view.show_error_out_list()
            else:
                break
        return x

    def player_y_select(self):
        while(True):
            while(True):  # try over and over untill player fill anwser
                try:
                    y = int(input('Please select y point : '))
                    break
                except ValueError:
                    self.view.show_error_non_number()
            if(y > 3 or y < 1):  # check error out of index
                self.view.show_error_out_list()
            else:
                break
        return y


class OX_controller_one_input(OX_controller):

    def __innit__(self, model, view):
        super().__init__(self, model, view)

    def player_selection(self):
        while(True):
            number_point = self.receive_number()
            x = int((number_point-1) % 3)
            y = int((9-number_point) / 3)
            if(self.model.check_filled(x, y)):
                if(self.model.check_player_turn() == 0):
                    self.model.fill_table(x, y, 'O')
                else:
                    self.model.fill_table(x, y, 'X')
                break
            else:
                self.view.show_error_filled()

    def receive_number(self):
        while(True):
            while(True):  # try over and over untill player fill anwser
                try:
                    number = int(input('Please select your number : '))
                    break
                except ValueError:
                    self.view.show_error_non_number()
            if(number > 9 or number < 1):  # check error out of value
                self.view.show_error_out_list()
            else:
                break
        return number

setup()
